# API Documentation for EnterpriseFlowsRepository


# Description

This project contains all openapi definitions.
From this definitions, JAVA Interfaces are generated and deployed.

# API OpenAPI

Follow this to see OpenAPI description

[Web site](https://enterpriseflowsrepository.github.io/api-definitions/)

# Build all JAR

|Open API|Generation|
|---|---|
|Applications|![Applications JAXRS](https://github.com/EnterpriseFlowsRepository/api-definitions/workflows/Applications-JAXRS/badge.svg?branch=dev)|
|Business|![Business JAXRS](https://github.com/EnterpriseFlowsRepository/api-definitions/workflows/Business-JAXRS/badge.svg?branch=dev)|
|Enterprise|![Enterprise JAXRS](https://github.com/EnterpriseFlowsRepository/api-definitions/workflows/Enterprise-JAXRS/badge.svg?branch=dev)|
|Infrastructure|![Infrastructure JAXRS](https://github.com/EnterpriseFlowsRepository/api-definitions/workflows/Infrastructure-JAXRS/badge.svg?branch=dev)|
|Mediations|![Mediations JAXRS](https://github.com/EnterpriseFlowsRepository/api-definitions/workflows/Mediations-JAXRS/badge.svg?branch=dev)|
|Properties|![Properties JAXRS](https://github.com/EnterpriseFlowsRepository/api-definitions/workflows/Properties-JAXRS/badge.svg?branch=dev)|
|Traces|![Traces JAXRS](https://github.com/EnterpriseFlowsRepository/api-definitions/workflows/Traces-JAXRS/badge.svg?branch=dev)|
|Transcodifications|![Transcodifications JAXRS](https://github.com/EnterpriseFlowsRepository/api-definitions/workflows/Transcodifications-JAXRS/badge.svg?branch=dev)|
