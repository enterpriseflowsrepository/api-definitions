echo "Module mocked: $EFR_MODULE"
export OPENAPI_URL="https://raw.githubusercontent.com/EnterpriseFlowsRepository/api-definitions/dev/api/$EFR_MODULE/v1/openapi.yaml"

echo "Download image"
# downloads an image
docker pull muonsoft/openapi-mock

echo "Start image"
docker run --detach \
    --name "mock-$EFR_MODULE" \
    -p $MOCK_PORT:8080 \
    --env "OPENAPI_MOCK_SPECIFICATION_URL=$OPENAPI_URL" \
    --rm \
    muonsoft/openapi-mock

echo "Mock started at http://localhost:$MOCK_PORT/"

# to test that the server successfully ran
#curl "http://localhost:$MOCK_PORT/contract"