# API for Enterprise Flow Repository

## Enterprise

[Version 1](enterprise-v1.html)

## Business

[Version 1](business-v1.html)

## Application

[Version 1](application-v1.html)

## Mediation

[Version 1](mediations-v1.html)

## Infrastructure

[Version 1](infrastructure-v1.html)

## Traces

[Version 1](traces-v1.html)

## Properties

[Version 1](properties-v1.html)

## Transcodifications

[Version 1](transcodifications-v1.html)
