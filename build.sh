#!/bin/sh
rm -rf target/
mkdir target/

../install_openapi-generator.sh
chmod u+x *.sh

OPENAPI_GENERATOR_VERSION=5.0.0-beta3
GENERATOR_SERVER=jaxrs-spec
GENERATOR_CLIENT=java

MAVEN_OPTIONS="-o"

for APINAME in applications business enterprise infrastructure mediations properties traces transcodifications
do
    echo "$APINAME:"
    echo "- $APINAME: validation"
    ./openapi-generator-cli.sh validate \
        -i ./api/$APINAME/v1/openapi.yaml \
        >> ./target/$APINAME.log

    echo "- $APINAME: server generation"
    ./openapi-generator-cli.sh generate \
        -i ./api/$APINAME/v1/openapi.yaml \
        -g $GENERATOR_SERVER \
        -o ./target/$APINAME/server \
        -c generate/$APINAME.yaml \
        >> ./target/$APINAME.log

    echo "- $APINAME: server install jar"
    mvn -f ./target/$APINAME/server/pom.xml \
        install \
        >> ./target/$APINAME.log
    
    echo "- $APINAME: client generation"
    ./openapi-generator-cli.sh generate \
        -i ./api/$APINAME/v1/openapi.yaml \
        -g $GENERATOR_CLIENT \
        -o ./target/$APINAME/client \
        -c generate/$APINAME.client.yaml \
        >> ./target/$APINAME.log

    echo "- $APINAME: client install jar"
    mvn -f ./target/$APINAME/client/pom.xml \
        -s settings.xml \
        -DaltDeploymentRepository=github::default::$MAVEN_REPO \
        deploy \
        >> ./target/$APINAME.log
done
